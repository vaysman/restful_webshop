package restful.dao;

import restful.model.Brand;

public class BrandDao extends AbstractDao<Brand> {
    public BrandDao(){
        super(Brand.class);
    }
}
