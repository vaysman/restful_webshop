package restful.dao;

        import restful.model.Product;

public class ProductDao extends AbstractDao<Product>{
    public ProductDao(){ super(Product.class);}
}
