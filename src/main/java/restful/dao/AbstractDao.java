package restful.dao;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import restful.util.HibernateUtil;
import java.util.List;

public  abstract class AbstractDao<T> {

    protected Class<T> genericClass;
    protected final SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
    public AbstractDao (Class<T> genericClass) {
        this.genericClass = genericClass;
    }
    public Class<T> getGenericClass() {
        return genericClass;
    }

    public void setGenericClass(Class<T> genericClass) {
        this.genericClass = genericClass;
    }

    public List<T> findAll() {
        List<T> entities = null;
        Session session = null;

        try {
            session = sessionFactory.openSession();
            session.beginTransaction();
            Query query = session.createQuery("from" + this.genericClass.getSimpleName()+ "e");
            entities = query.list();
            session.getTransaction().commit();
        }
        catch (Exception ex) {
            if (session !=null) {
                session.getTransaction().rollback();
            }
        }
        finally {
            if(session != null){
                session.close();
            }
        }
        return entities;
    }

    public T get( int id) {
        T entity = null;
        Session session = null;
        try {
            session = sessionFactory.openSession();
            session.beginTransaction();
            Query query = session.createQuery("from " + this.genericClass.getSimpleName()+ " e where e.id = :ID ");
            query.setParameter("ID", id);
            entity = (T) query.uniqueResult();
            Hibernate.initialize(entity);
            session.getTransaction().commit();
        }
        catch (Throwable ex) {
            if (session !=null) {
                session.getTransaction().rollback();
            }
        }
        finally {
            if(session != null){
                session.close();
            }
        }
        return entity;
    }


    public boolean create (T entity) {

        Boolean success = false;
        Session session = null;

        try {
            session = sessionFactory.openSession();
            session.beginTransaction();
            session.persist(entity);
            session.getTransaction().commit();
            success = true;
        }
        catch (Exception exception) {
            exception.printStackTrace();
            if (session != null) {
                session.getTransaction().rollback();
            }
        }
        finally {
            if (session != null) {
                session.close();
            }
        }
        return success;
    }
}
